package oak.forge.commons.projections;

import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.bus.Subscribers;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.query.filter.Filters;
import oak.forge.commons.data.readmodel.InvalidReadModelException;
import oak.forge.commons.data.readmodel.ReadModel;
import oak.forge.commons.data.readmodel.ReadModelStorage;
import oak.forge.commons.data.version.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.lang.String.format;

public abstract class DefaultProjection<M extends ReadModel> implements Projection<M> {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultProjection.class);

	private final String readModelName;
	private final Supplier<M> newInstanceSupplier;
	private final ReadModelStorage readModelStorage;
	private final MessageBus messageBus;

	private final Map<Class<? extends Event>, BiFunction<? extends Event, M, M>> handlers;

	public DefaultProjection(String readModelName, Supplier<M> newInstanceSupplier, ReadModelStorage readModelStorage, MessageBus messageBus) {
		this.readModelName = readModelName;
		this.newInstanceSupplier = newInstanceSupplier;
		this.readModelStorage = readModelStorage;
		this.messageBus = messageBus;
		this.handlers = new HashMap<>();
	}

	@Override
	public <E extends Event> void registerHandler(Class<E> eventClass, BiFunction<E, M, M> handler) {
		handlers.put(eventClass, handler);
		messageBus.subscribe(eventClass, Subscribers.consumer((Consumer<E>) this::handle));
	}

	@Override
	public <E extends Event> void handle(E event) {
		BiFunction<E, M, M> handler = (BiFunction<E, M, M>) handlers.get(event.getClass());
		if (handler == null) {
			LOG.warn("no projection handler for event [{}] found", event.getClass().getName());
			return;
		}
		M model = getReadModel(event.getAggregateId());
		validateIncomingEventAgainstModel(model, event.getAggregateId(), event.getVersion());
		M result = handler.apply(event, model);
		if (result == null) {
			readModelStorage.delete(readModelName, Filters.aggregateId(event.getAggregateId()));
		} else {
			storeRecent(result, event.getTimestamp(), event.getVersion());
		}
	}

	private M getReadModel(UUID aggregateId) {
		List<M> results = readModelStorage.get(readModelName, Filters.aggregateId(aggregateId));
		if (!results.isEmpty()) {
			return results.get(0);
		}
		return newModelInstance(aggregateId);
	}

	private M newModelInstance(UUID aggregateId) {
		M model = newInstanceSupplier.get();
		model.setAggregateId(aggregateId);
		model.setVersion(Version.initial());
		return model;
	}

	private void validateIncomingEventAgainstModel(M model, UUID incomingAggregateId, Version incomingVersion) {
		if (model.getAggregateId() == null
				|| model.getVersion() == null) {
			String message = "read model is invalid";
			throw new InvalidReadModelException(message);
		}

		if (!incomingAggregateId.equals(model.getAggregateId())) {
			String message = format("incoming event aggregate id: [%s] is different than read model aggregate id: [%s]",
					incomingAggregateId, model.getAggregateId());
			throw new InvalidReadModelException(message);
		}
		if (incomingVersion.compareTo(model.getVersion()) <= 0) {
			String message = format("incoming event version: [%s] is lower or equal to read model version: [%s]",
					incomingVersion, model.getVersion());
			throw new InvalidReadModelException(message);
		}
	}

	private void storeRecent(M model, Instant creationTimestamp, Version version) {
		model.setCreationTimestamp(creationTimestamp);
		model.setVersion(version);
		readModelStorage.store(readModelName, model, version);
	}
}
