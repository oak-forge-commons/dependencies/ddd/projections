package oak.forge.commons.projections;

public class NoSuchProjectionException extends RuntimeException {

	private static final long serialVersionUID = -5656497793463924703L;

	public NoSuchProjectionException(String message) {
		super(message);
	}
}
