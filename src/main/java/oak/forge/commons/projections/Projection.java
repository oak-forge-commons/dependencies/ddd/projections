package oak.forge.commons.projections;

import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.readmodel.ReadModel;

import java.util.function.BiFunction;

public interface Projection<M extends ReadModel> {

	<E extends Event> void registerHandler(Class<E> eventClass, BiFunction<E, M, M> handler);

	<E extends Event> void handle(E event);

}
