package oak.forge.commons.projections;

import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.readmodel.InvalidReadModelException;
import oak.forge.commons.data.readmodel.ReadModel;
import oak.forge.commons.data.readmodel.ReadModelStorage;
import oak.forge.commons.data.version.Version;
import oak.forge.commons.projections.test.TestEvent;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.UUID;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultProjectionTest {

    private static final String NAME = "test-projection";

    private final ReadModelStorage readModelStorage = mock(ReadModelStorage.class);
    private final MessageBus messageBus = mock(MessageBus.class);

    private final Model newInstanceFromSupplier = new Model();
    private Model currentModel = null;

    private final Projection<Model> projection = new DefaultProjection<Model>(NAME, () -> newInstanceFromSupplier, readModelStorage, messageBus) {
        {
            registerHandler(TestEvent.class, this::handle);
        }

        private Model handle(TestEvent event, Model model) {
            currentModel = model;
            return model;
        }
    };

    private static class Model extends ReadModel {
        private static final long serialVersionUID = 1L;

    }

    private static class NotRegisteredEvent extends Event {
        private static final long serialVersionUID = 1L;
    }

    @Test
    public void should_not_handle_not_registered_event() {
        // given
        NotRegisteredEvent event = new NotRegisteredEvent();

        // when
        projection.handle(event);

        // then
        assertThat(currentModel).isNull();
    }

    @Test
    public void should_update_single_instance() {
        // given
        UUID aggregateId = randomUUID();
        TestEvent event = new TestEvent(aggregateId, 1, aggregateId);

        // when
        projection.handle(event);

        // then
        assertThat(currentModel).isSameAs(newInstanceFromSupplier);
        assertThat(currentModel.getAggregateId()).isEqualTo(event.getAggregateId());
        Assertions.assertThat(currentModel.getVersion()).isEqualTo(Version.of(1));

    }

    @Test
    public void should_not_accept_events_when_read_model_has_no_aggregate_id() {
        //given
        UUID aggregateId = randomUUID();
        TestEvent event = new TestEvent(aggregateId, 1, aggregateId);

        ReadModel model = new Model();
        model.setVersion(Version.initial());

        when(readModelStorage.get(eq(NAME), any())).thenReturn(singletonList(model));

        String expectedErrorMessage = "read model is invalid";

        //then
        assertThatExceptionOfType(InvalidReadModelException.class)
                .isThrownBy(() -> projection.handle(event))
                .withMessage(expectedErrorMessage);
    }

    @Test
    public void should_not_accept_events_when_read_model_has_no_version() {
        //given
        UUID aggregateId = randomUUID();
        TestEvent event = new TestEvent(aggregateId, 1, aggregateId);

        ReadModel model = new Model();
        model.setAggregateId(aggregateId);

        when(readModelStorage.get(eq(NAME), any())).thenReturn(singletonList(model));

        String expectedErrorMessage = "read model is invalid";

        //then
        assertThatExceptionOfType(InvalidReadModelException.class)
                .isThrownBy(() -> projection.handle(event))
                .withMessage(expectedErrorMessage);
    }

    @Test
    public void should_not_accept_events_of_different_aggregate_id_than_model() {
        //given
        UUID modelAggregateId = randomUUID();
        UUID eventAggregateId = randomUUID();

        ReadModel model = new Model();
        model.setAggregateId(modelAggregateId);
        model.setVersion(Version.initial());

        TestEvent event = new TestEvent(eventAggregateId, 1, eventAggregateId);

        when(readModelStorage.get(eq(NAME), any())).thenReturn(singletonList(model));

        String expectedErrorMessage = "incoming event aggregate id: [%s] is different than read model aggregate id: [%s]";

        //then
        assertThatExceptionOfType(InvalidReadModelException.class)
                .isThrownBy(() -> projection.handle(event))
                .withMessage(format(expectedErrorMessage, eventAggregateId, modelAggregateId));
    }

    @Test
    public void should_not_accept_events_with_version_lower_or_equal_to_model_version() {
        //given
        UUID aggregateId = randomUUID();

        int modelVersion = 10;
        int firstEventVersion = 8;
        int secondEventVersion = 10;

        ReadModel model = new Model();
        model.setAggregateId(aggregateId);
        model.setVersion(Version.of(modelVersion));

        TestEvent firstEvent = new TestEvent(aggregateId, firstEventVersion, aggregateId);
        TestEvent secondEvent = new TestEvent(aggregateId, secondEventVersion, aggregateId);

        when(readModelStorage.get(eq(NAME), any())).thenReturn(singletonList(model));

        String expectedErrorMessage = "incoming event version: [%s] is lower or equal to read model version: [%s]";

        //then
        assertThatExceptionOfType(InvalidReadModelException.class)
                .isThrownBy(() -> projection.handle(firstEvent))
                .withMessage(format(expectedErrorMessage, firstEventVersion, modelVersion));
        assertThatExceptionOfType(InvalidReadModelException.class)
                .isThrownBy(() -> projection.handle(secondEvent))
                .withMessage(format(expectedErrorMessage, secondEventVersion, modelVersion));
    }
}
